package com.alysenko.word.deduplication.controller;

import com.alysenko.word.deduplication.exception.FileNotFoundException;
import com.alysenko.word.deduplication.service.FileService;
import com.alysenko.word.deduplication.service.WordDeduplicationService;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.BufferedReader;
import java.io.StringReader;

import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest
@AutoConfigureMockMvc
public class FileReadRestControllerTest {

    @Autowired
    private FileReadRestController fileReadRestController;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FileService fileServiceMock;

    @MockBean
    private WordDeduplicationService wordDeduplicationServiceMock;

    @Test
    public void shouldFetchExistingFile() throws Exception {
        BufferedReader responseMock = new BufferedReader(new StringReader("abc"));
        when(fileServiceMock.getProcessedFile(anyString())).thenReturn(responseMock);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/file/existing_file.txt"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_OCTET_STREAM))
                .andExpect(content().string("abc"));
    }

    @Test
    public void shouldReturnNotFoundForNonExistingFile() throws Exception {
        doThrow(new FileNotFoundException("Not found")).when(fileServiceMock).getProcessedFile(anyString());

        mockMvc.perform(MockMvcRequestBuilders.get("/api/file/non_existing_file.txt"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldReturnServerErrorWhenUnexpectedErrorOccurred() throws Exception {
        doThrow(new RuntimeException("Error")).when(fileServiceMock).getProcessedFile(anyString());

        mockMvc.perform(MockMvcRequestBuilders.get("/api/file/file.txt"))
                .andExpect(status().isInternalServerError())
        .andExpect(content().contentType(APPLICATION_JSON))
        .andExpect(jsonPath("message", Is.is("Error")));
    }
}
