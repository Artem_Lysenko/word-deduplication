package com.alysenko.word.deduplication.controller;

import com.alysenko.word.deduplication.service.FileService;
import com.alysenko.word.deduplication.service.WordDeduplicationService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;

import static org.mockito.Mockito.*;

@WebMvcTest
@AutoConfigureMockMvc
public class FileProcessRestControllerTest {

    @Autowired
    private FileProcessRestController fileProcessRestController;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FileService fileServiceMock;

    @MockBean
    private WordDeduplicationService wordDeduplicationServiceMock;


    @Test
    public void shouldUploadFileSuccessfully() throws Exception {
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("sample/sample-file.txt");
        MockMultipartFile multipartFileMock = new MockMultipartFile(
                "file",
                "sample-file.txt",
                "multipart/form-data",
                inputStream);
        MockMultipartHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .multipart("/api/file/sample-file.txt");
        requestBuilder.with(request -> {
            request.setMethod("PUT");
            return request;
        });

        mockMvc.perform(requestBuilder.file(multipartFileMock))
                .andExpect(MockMvcResultMatchers.status().isOk());

        verify(fileServiceMock, times(1))
                .uploadFile(eq("sample-file.txt"), any(HttpServletRequest.class));
        verify(wordDeduplicationServiceMock, times(1))
                .deduplicateWordsOfFile("sample-file.txt");
    }
}
