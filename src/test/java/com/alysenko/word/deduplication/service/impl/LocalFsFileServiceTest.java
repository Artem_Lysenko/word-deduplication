package com.alysenko.word.deduplication.service.impl;

import com.alysenko.word.deduplication.exception.WrongUploadRequestException;
import com.alysenko.word.deduplication.repository.FileRepository;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;

import static java.util.Arrays.asList;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class LocalFsFileServiceTest {

    private LocalFsFileService testInstance;

    @Mock
    private HttpServletRequest httpServletRequestMock;

    @Mock
    private FileRepository fileRepositoryMock;

    @Mock
    private ServletFileUpload servletFileUploadMock;

    @Mock
    private FileItem fileItemA;

    @Mock
    private FileItem fileItemB;

    @Before
    public void init() {
        testInstance = spy(new LocalFsFileService(
                "./uploads/",
                "./processed/",
                fileRepositoryMock));
        doReturn(servletFileUploadMock).when(testInstance).buildServletFileUpload();
    }

    @Test(expected = WrongUploadRequestException.class)
    public void shouldThrowWrongUploadExceptionWhenUploadIfWrongFileNameSpecified() {
        testInstance.uploadFile("#$%#$%.txt", httpServletRequestMock);
    }

    @Test(expected = WrongUploadRequestException.class)
    public void shouldThrowWrongUploadExceptionWhenNoFilesArePassed() {
        doReturn(asList(fileItemA, fileItemB))
                .when(testInstance).parseHttpServletRequest(
                any(HttpServletRequest.class), eq(servletFileUploadMock));

        testInstance.uploadFile("sample-file.txt", httpServletRequestMock);
    }

    @Test
    public void shouldSuccessfullyUploadFile() {
        doReturn(asList(fileItemA))
                .when(testInstance).parseHttpServletRequest(any(HttpServletRequest.class), eq(servletFileUploadMock));
        doNothing().when(testInstance).saveFile(eq("sample-file.txt"), eq(fileItemA));

        testInstance.uploadFile("sample-file.txt", httpServletRequestMock);

        verify(testInstance, times(1)).saveFile(eq("sample-file.txt"), eq(fileItemA));
    }
}
