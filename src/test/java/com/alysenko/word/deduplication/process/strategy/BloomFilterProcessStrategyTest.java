package com.alysenko.word.deduplication.process.strategy;

public class BloomFilterProcessStrategyTest extends AbstractProcessStrategyTest {

    @Override
    public ProcessStrategy getStrategy() {
        return new BloomFilterBasedProcessStrategy();
    }
}
