package com.alysenko.word.deduplication.process.strategy;

import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.StringReader;
import java.io.StringWriter;

public abstract class AbstractProcessStrategyTest {

    @Test
    public void shouldDeduplicateSingleLineFile() {
        BufferedReader source = new BufferedReader(new StringReader("abc"));
        StringWriter stringWriter = new StringWriter();
        BufferedWriter target = new BufferedWriter(stringWriter);

        getStrategy().process(source, target);

        Assert.assertEquals("abc", stringWriter.toString());
    }

    @Test
    public void shouldDeduplicateEmptyFile() {
        BufferedReader source = new BufferedReader(new StringReader(""));
        StringWriter stringWriter = new StringWriter();
        BufferedWriter target = new BufferedWriter(stringWriter);

        getStrategy().process(source, target);

        Assert.assertEquals("", stringWriter.toString());
    }

    @Test
    public void shouldDeduplicate() {
        BufferedReader source = new BufferedReader(new StringReader("a\nb\nc\na\nc\nd"));
        StringWriter stringWriter = new StringWriter();
        BufferedWriter target = new BufferedWriter(stringWriter);

        getStrategy().process(source, target);

        Assert.assertEquals("a\nb\nc\nd", stringWriter.toString());
    }

    public abstract ProcessStrategy getStrategy();
}
