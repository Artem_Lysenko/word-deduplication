package com.alysenko.word.deduplication.process.strategy;

public class HashSetBasedProcessStrategyTest extends AbstractProcessStrategyTest {

    @Override
    public ProcessStrategy getStrategy() {
        return new HashSetBasedProcessStrategy();
    }
}
