package com.alysenko.word.deduplication;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WordDeduplicationIntegrationTest {

    @LocalServerPort
    private int port;

    private TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void shouldSubmitFileForProcessing() throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        InputStream inputStream = getResource("sample/input/siden_coding_test_file_sample.txt");
        body.add("file", inputStream.readAllBytes());
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                buildUri("/api/file/sample-file.txt"), HttpMethod.PUT, requestEntity, String.class);

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    private InputStream getResource(String fileName) {
        return this.getClass().getClassLoader().getResourceAsStream(fileName);
    }

    @After
    public void tierDown() throws IOException {
        FileUtils.deleteDirectory(new File("it-data"));
    }

    private String buildUri(String uri) {
        return "http://localhost:" + port + uri;
    }
}
