package com.alysenko.word.deduplication.exception;

public class WrongUploadRequestException extends RuntimeException {

    public WrongUploadRequestException() {
    }

    public WrongUploadRequestException(String message) {
        super(message);
    }
}
