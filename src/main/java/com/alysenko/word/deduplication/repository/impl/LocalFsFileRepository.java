package com.alysenko.word.deduplication.repository.impl;

import com.alysenko.word.deduplication.exception.FileNotFoundException;
import com.alysenko.word.deduplication.repository.FileRepository;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

@Component
public class LocalFsFileRepository implements FileRepository {

    @Override
    public BufferedReader getBufferedReaderOfFile(String fileLocation) {
        Path path = Paths.get(fileLocation);
        if (! Files.exists(path)) {
            throw new FileNotFoundException("File " + fileLocation + " does not exists");
        }

        return getBufferedReader(path);
    }

    @Override
    public BufferedWriter getBufferedWriterOfFile(String fileName) {
        return getBufferedWriter(fileName);
    }

    @SneakyThrows
    private BufferedWriter getBufferedWriter(String fileName) {
        return Files.newBufferedWriter(
                Paths.get(fileName),
                StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);
    }

    @SneakyThrows
    private BufferedReader getBufferedReader(Path path) {
        return Files.newBufferedReader(path);
    }
}
