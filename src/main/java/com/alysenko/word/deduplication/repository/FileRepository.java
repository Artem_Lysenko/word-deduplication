package com.alysenko.word.deduplication.repository;

import java.io.BufferedReader;
import java.io.BufferedWriter;

public interface FileRepository {
    BufferedReader getBufferedReaderOfFile(String fileName);
    BufferedWriter getBufferedWriterOfFile(String fileName);
}
