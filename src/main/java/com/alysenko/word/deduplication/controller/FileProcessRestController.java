package com.alysenko.word.deduplication.controller;

import com.alysenko.word.deduplication.data.dto.CommonResponseMessage;
import com.alysenko.word.deduplication.service.FileService;
import com.alysenko.word.deduplication.service.WordDeduplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class FileProcessRestController {

    private final FileService fileService;
    private final WordDeduplicationService wordDeduplicationService;

    @Autowired
    public FileProcessRestController(FileService fileService,
                                     WordDeduplicationService wordDeduplicationService) {
        this.fileService = fileService;
        this.wordDeduplicationService = wordDeduplicationService;
    }

    @PutMapping(path = "/api/file/{fileName}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<CommonResponseMessage> uploadFile(@PathVariable("fileName") String fileName,
                                                            HttpServletRequest httpServletRequest) {
        fileService.uploadFile(fileName, httpServletRequest);
        wordDeduplicationService.deduplicateWordsOfFile(fileName);
        return ResponseEntity.ok(new CommonResponseMessage("File successfully processed"));
    }
}
