package com.alysenko.word.deduplication.controller;

import com.alysenko.word.deduplication.service.FileService;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.ReaderInputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.InputStream;

@RestController
public class FileReadRestController {

    private final FileService fileService;

    @Autowired
    public FileReadRestController(FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping(path = "/api/file/{fileName}")
    public void fetchFile(@PathVariable("fileName") String fileName, HttpServletResponse response) {
        BufferedReader processedFile = fileService.getProcessedFile(fileName);
        InputStream inputStream = new ReaderInputStream(processedFile, "UTF-8");
        response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
        flushInputStreamToResponse(response, inputStream);
    }

    @SneakyThrows
    private void flushInputStreamToResponse(HttpServletResponse response, InputStream inputStream) {
        IOUtils.copy(inputStream, response.getOutputStream());
    }
}
