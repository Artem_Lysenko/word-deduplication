package com.alysenko.word.deduplication.controller;

import com.alysenko.word.deduplication.data.dto.CommonResponseMessage;
import com.alysenko.word.deduplication.exception.FileNotFoundException;
import com.alysenko.word.deduplication.exception.WrongUploadRequestException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@Slf4j
@ControllerAdvice
public class ErrorControllerAdvice {

    @ExceptionHandler(WrongUploadRequestException.class)
    public ResponseEntity<CommonResponseMessage> handleWrongPayload(WrongUploadRequestException exception) {
        log.info("Failed to upload file: " + exception.getMessage());

        return ResponseEntity
                .badRequest()
                .body(new CommonResponseMessage(exception.getMessage()));
    }

    @ExceptionHandler(FileNotFoundException.class)
    public ResponseEntity<CommonResponseMessage> handleFileNotFound(FileNotFoundException exception) {
        log.info("Not found: " + exception.getMessage());

        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(Throwable.class)
    public ResponseEntity<CommonResponseMessage> handleAnyThrowable(Throwable throwable) {
        log.error("Internal error occurred", throwable);
        return ResponseEntity
                .status(INTERNAL_SERVER_ERROR)
                .body(new CommonResponseMessage(throwable.getMessage()));
    }
}
