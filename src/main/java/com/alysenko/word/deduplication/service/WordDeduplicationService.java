package com.alysenko.word.deduplication.service;

public interface WordDeduplicationService {
    void deduplicateWordsOfFile(String fileName);
}
