package com.alysenko.word.deduplication.service.impl;

import com.alysenko.word.deduplication.process.strategy.ProcessStrategies;
import com.alysenko.word.deduplication.repository.FileRepository;
import com.alysenko.word.deduplication.service.WordDeduplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.BufferedWriter;

import static java.util.Optional.ofNullable;

@Service
public class WordDeduplicationServiceImpl implements WordDeduplicationService {

    public static final ProcessStrategies DEFAULT_PROCESS_STRATEGY = ProcessStrategies.HASH_SET;

    private FileRepository fileRepository;
    private String uploadFolder;
    private String processedFolder;
    private String processStrategyName;
    private ProcessStrategies processStrategy;

    @Autowired
    public WordDeduplicationServiceImpl(
            @Value("${app.folder.upload}") String uploadFolder,
            @Value("${app.folder.processed}") String processedFolder,
            @Value("${app.process.strategy}") String processStrategyName,
            FileRepository fileRepository) {

        this.uploadFolder = uploadFolder;
        this.processedFolder = processedFolder;
        this.processStrategyName = ofNullable(processStrategyName).orElse(DEFAULT_PROCESS_STRATEGY.name());
        this.fileRepository = fileRepository;
    }

    @PostConstruct
    public void init() {
        processStrategy = ProcessStrategies.valueOf(processStrategyName.toUpperCase());
    }

    @Override
    public void deduplicateWordsOfFile(String fileName) {
        BufferedReader source = fileRepository.getBufferedReaderOfFile(uploadFolder + fileName);
        BufferedWriter dest = fileRepository.getBufferedWriterOfFile(processedFolder + fileName);
        processStrategy.getStrategy().process(source, dest);
    }
}
