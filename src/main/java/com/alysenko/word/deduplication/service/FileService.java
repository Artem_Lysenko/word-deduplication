package com.alysenko.word.deduplication.service;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;

public interface FileService {
    void uploadFile(String fileName, HttpServletRequest httpServletRequest);
    BufferedReader getProcessedFile(String fileName);
}
