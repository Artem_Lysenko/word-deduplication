package com.alysenko.word.deduplication.service.impl;

import com.alysenko.word.deduplication.exception.WrongUploadRequestException;
import com.alysenko.word.deduplication.repository.FileRepository;
import com.alysenko.word.deduplication.service.FileService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import lombok.SneakyThrows;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.List;

@Slf4j
@Service
public class LocalFsFileService implements FileService {

    public static final String PROP_JAVA_IO_TMPDIR = "java.io.tmpdir";

    public static final String FILE_NAME_PATTERN = "[\\w\\-. ]+";

    private final String fileUploadFolder;
    private final String fileProcessFolder;
    private final FileRepository fileRepository;

    @Autowired
    public LocalFsFileService(@Value("${app.folder.upload}") String fileUploadFolder,
                              @Value("${app.folder.processed}") String fileProcessFolder,
                              FileRepository fileRepository) {
        this.fileUploadFolder = fileUploadFolder;
        this.fileProcessFolder = fileProcessFolder;
        this.fileRepository = fileRepository;
    }

    @PostConstruct
    public void init() {
        createFolderIfNotExists(fileUploadFolder);
        createFolderIfNotExists(fileProcessFolder);
    }

    @Override
    public void uploadFile(String fileName, HttpServletRequest httpServletRequest) {
        validateFileName(fileName);
        ServletFileUpload upload = buildServletFileUpload();
        List<FileItem> fileItems = parseHttpServletRequest(httpServletRequest, upload);
        checkFileItemsExactOne(fileItems);
        saveFile(fileName, fileItems.get(0));
    }

    protected ServletFileUpload buildServletFileUpload() {
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setRepository(new File(System.getProperty(PROP_JAVA_IO_TMPDIR)));
        factory.setSizeThreshold(DiskFileItemFactory.DEFAULT_SIZE_THRESHOLD);
        factory.setFileCleaningTracker(null);
        return new ServletFileUpload(factory);
    }

    @Override
    public BufferedReader getProcessedFile(String fileName) {
        return fileRepository.getBufferedReaderOfFile(fileProcessFolder + fileName);
    }

    private void validateFileName(String fileName) {
        if (! fileName.matches(FILE_NAME_PATTERN)) {
            throw new WrongUploadRequestException("Invalid name of file specified");
        }
    }

    private void checkFileItemsExactOne(List<FileItem> fileItems) {
        if (fileItems.size() != 1) {
            throw new WrongUploadRequestException("Exactly one file expected, but received " + fileItems.size());
        }
    }

    @SneakyThrows
    protected void saveFile(String fileName, FileItem fileItem) {
        String outputFileName = fileUploadFolder + fileName;
        try (InputStream uploadedStream = fileItem.getInputStream();
             OutputStream out = new FileOutputStream(outputFileName)) {

            IOUtils.copy(uploadedStream, out);
        }
    }

    protected List<FileItem> parseHttpServletRequest(HttpServletRequest httpServletRequest, ServletFileUpload upload) {
        try {
            return upload.parseRequest(httpServletRequest);
        } catch (Throwable th) {
            throw new WrongUploadRequestException("Wrong payload: " + th.getMessage());
        }
    }

    public void createFolderIfNotExists(String folderName) {
        File directory = new File(folderName);
        if (! directory.exists()){
            log.info("Initializing folder " + folderName + " does not exists");
            directory.mkdirs();
        }
    }
}
