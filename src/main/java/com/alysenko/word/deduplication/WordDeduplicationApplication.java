package com.alysenko.word.deduplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WordDeduplicationApplication {

	public static void main(String[] args) {
		SpringApplication.run(WordDeduplicationApplication.class, args);
	}
}
