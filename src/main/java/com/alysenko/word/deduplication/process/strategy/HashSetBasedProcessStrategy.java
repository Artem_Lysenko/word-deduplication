package com.alysenko.word.deduplication.process.strategy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.HashSet;
import java.util.Set;

public class HashSetBasedProcessStrategy extends AbstractProcessStrategy implements ProcessStrategy {

    @Override
    public void process(BufferedReader source, BufferedWriter dest) {
        Set<String> processedWord = new HashSet<>();
        String prevProcessedLine = null;
        String curLine = nextLine(source);
        while (curLine != null) {
            if (! processedWord.contains(curLine)) {
                processedWord.add(curLine);
                writeLine(dest, curLine, prevProcessedLine);
                prevProcessedLine = curLine;
            }
            curLine = nextLine(source);
        }

        flushAndClose(dest);
    }
}
