package com.alysenko.word.deduplication.process.strategy;

import lombok.SneakyThrows;

import java.io.BufferedReader;
import java.io.BufferedWriter;

public abstract class AbstractProcessStrategy implements ProcessStrategy {

    @SneakyThrows
    protected void writeLine(BufferedWriter dest, String curLine, String prevProcessedLine) {
        if (prevProcessedLine != null) {
            dest.newLine();
        }
        dest.write(curLine);
    }

    @SneakyThrows
    protected String nextLine(BufferedReader source) {
        return source.readLine();
    }

    @SneakyThrows
    protected void flushAndClose(BufferedWriter dest) {
        dest.flush();
        dest.close();
    }
}
