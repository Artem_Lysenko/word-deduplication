package com.alysenko.word.deduplication.process.strategy;

import java.io.BufferedReader;
import java.io.BufferedWriter;

public interface ProcessStrategy {
    void process(BufferedReader source, BufferedWriter dest);
}
