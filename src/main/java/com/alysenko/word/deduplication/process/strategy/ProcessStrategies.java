package com.alysenko.word.deduplication.process.strategy;

import java.util.function.Supplier;

public enum ProcessStrategies {
    HASH_SET(() -> new HashSetBasedProcessStrategy()),
    BLOOM_FILTER(() -> new BloomFilterBasedProcessStrategy());

    ProcessStrategies(Supplier<ProcessStrategy> strategySupplier) {
        this.strategySupplier = strategySupplier;
    }

    Supplier<ProcessStrategy> strategySupplier;

    public ProcessStrategy getStrategy() {
        return strategySupplier.get();
    }
}
