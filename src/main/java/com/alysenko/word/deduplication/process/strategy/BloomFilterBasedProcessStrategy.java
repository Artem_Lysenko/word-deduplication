package com.alysenko.word.deduplication.process.strategy;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnel;
import com.google.common.hash.Funnels;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.nio.charset.Charset;

public class BloomFilterBasedProcessStrategy extends AbstractProcessStrategy implements ProcessStrategy {

    @Override
    public void process(BufferedReader source, BufferedWriter dest) {
        Funnel<CharSequence> funnel = Funnels.stringFunnel(Charset.forName("UTF-8"));
        BloomFilter<String> filter = BloomFilter.create(funnel, 1000000);

        String prevProcessedLine = null;
        String curLine = nextLine(source);
        while (curLine != null) {
            if (!filter.mightContain(curLine)) {
                filter.put(curLine);
                writeLine(dest, curLine, prevProcessedLine);
                prevProcessedLine = curLine;
            }
            curLine = nextLine(source);
        }

        flushAndClose(dest);
    }
}
