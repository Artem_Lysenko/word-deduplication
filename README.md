# Fullstack Coding Task

### Description
Logically application is broken on three layers: presentation, business, and data-access. 
So, multi-tier (three-tier) architecture is applied. It's a common approach for the building most of the business applications.
The architecture ensures model, which can be used to develop simple, reusable, and maintainable applications. 

### Technologies
Application is implemented using Java 11, SpringBoot 2.2.6 and Gradle 5.2.1

Since Java 11 supports cgroups, applications, which run in Java 11 can be easily wrapped into the Docker image.
SpringFramework was chosen, because it's modern, mature enough, reliable, and cloud-friendly, which is important for modern applications. It's supplied with various modules, which integrate seamlessly. Spring testing framework provides great testing capabilities. 
### Test & Run

* To run Unit Tests ```./gradlew test```
* To run integration test: ```./gradlew integrationTest``` 
* To run application using Gradle: ```./gradlew bootRun```
